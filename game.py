def print_board(entries):
    line = "+---+---+---+"
    output = line
    n = 0
    for entry in entries:
        if n % 3 == 0:
            output = output + "\n| "
        else:
            output = output + " | "
        output = output + str(entry)
        if n % 3 == 2:
            output = output + " |\n"
            output = output + line
        n = n + 1
    print(output)
    print()

def game_over(arr):
    print_board(board)
    print(arr, 'has won')
    print('GAME OVER')
    exit()

#Determines winner
def is_winner():
    if board[0] == board[1] and board[1] == board[2]:
        board_winner = 0
        return True
    elif board[3] == board[4] and board[4] == board[5]:
        board_winner = 3
        return True
    elif board[6] == board[7] and board[7] == board[8]:
        board_winner = 3
        return True
    elif board[0] == board[3] and board[3] == board[6]:
        board_winner = 0
        return True
    elif board[1] == board[4] and board[4] == board[7]:
        board_winner = 1
        return True
    elif board[2] == board[5] and board[5] == board[8]:
        board_winner = 2
        return True
    elif board[0] == board[4] and board[4] == board[8]:
        board_winner = 0
        return True
    elif board[2] == board[4] and board[4] == board[6]:
        board_winner = 2
        return True


board = [1, 2, 3, 4, 5, 6, 7, 8, 9]
current_player = "X"
board_winner = 0

for move_number in range(1, 10):
    print_board(board)
    response = input("Where would " + current_player + " like to move? ")
    space_number = int(response) - 1
    board[space_number] = current_player

    #check for winners
    if is_winner() == True:
        game_over(board[board_winner])

    if current_player == "X":
        current_player = "O"
    else:
        current_player = "X"

print("It's a tie!")
